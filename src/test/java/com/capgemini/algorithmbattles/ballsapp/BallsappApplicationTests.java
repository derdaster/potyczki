package com.capgemini.algorithmbattles.ballsapp;

import com.capgemini.algorithmbattles.ballsapp.logic.model.Board;
import com.capgemini.algorithmbattles.ballsapp.logic.model.BoardCell;
import com.capgemini.algorithmbattles.ballsapp.logic.model.Player;
import com.capgemini.algorithmbattles.ballsapp.solution.GamePlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BallsappApplicationTests {

	@Test
	public void contextLoads() {
		GamePlayer gamePlayer1 = new GamePlayer(Player.PLAYER_1);
		GamePlayer gamePlayer2 = new GamePlayer(Player.PLAYER_2);

		while(gamePlayer1.isEndGame() || gamePlayer2.isEndGame()){
			BoardCell player1Move = gamePlayer1.nextMove();
			gamePlayer2.moveMadeByOpponent(player1Move);

			BoardCell player2Move = gamePlayer2.nextMove();
			gamePlayer1.moveMadeByOpponent(player2Move);
		}

	}

}
