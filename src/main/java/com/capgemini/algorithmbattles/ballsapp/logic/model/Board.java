package com.capgemini.algorithmbattles.ballsapp.logic.model;

import java.util.*;

public class Board {

	private static final int size = 10;
	private Player[][] board = new Player[size][size];
	private double[][] weightBoard = new double[size][size];

    private int [][] m = {{-1,-1},{-1,0},{-1,1},{0,1},{0,-1},{1,-1},{1,0},{1,1}};
	public void placeMove(BoardCell move) {
		board[move.getX()][move.getY()] = move.getPlayer();
	}

	public BoardCell getFirstEmptyCell() {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] == null) {
					return new BoardCell(i, j, null);
				}
			}
		}
		return null;
	}

	public BoardCell getHighestRiskCell() {
		
		return new BoardCell(1,2, null);
	}


	private int getLineLength(int x, int y, int direction)
	{
        int length = 0;
        int next_x = x;
        int next_y = y;

        while(board[next_x][next_y]!=null) {
            next_x = x + m[direction][0];
            next_y = y + m[direction][1];
            if (next_x >= 0 && next_x < size && next_y >= 0 && next_y < size)
                length++;
            else return length;
            }


		return length;
	}
	
	public BoardCell getRandomCell()
	{
        /*if(isBoardEmpty()){
            return new BoardCell(5, 5,null);
        }*/


	    initializeWeightBoard();

	    HashMap<Integer,ArrayList<Integer>> hashMap = new HashMap<>();
		ArrayList<Double> weights = new ArrayList<>();
		for(int k = 0; k<size; k++){
		    for(int g = 0; g<size; g++){
		        weights.add(weightBoard[k][g]*(-1));
            }
        }

        double value = Collections.max(weights);
        int j = 0;
        for(int k = 0; k<size; k++){
            for(int g =0; g<size; g++){
                if(weightBoard[k][g]*(-1) == value){
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    arrayList.add(k);
                    arrayList.add(g);
                    hashMap.put(j, arrayList);
                    j++;
                }

            }
        }

        Random rand = new Random();

        int i = rand.nextInt(hashMap.size());
        System.out.println("x: " + hashMap.get(i).get(0) + " Y: " + hashMap.get(i).get(1));
        return new BoardCell(hashMap.get(i).get(0), hashMap.get(i).get(1),null);
	}

	private boolean isBoardEmpty(){
	    for(int k = 0; k<size; k++){
            for(int g = 0; g<size; g++){
                if(board[k][g]!=null)
                    return false;
            }
        }
        return true;
    }

	private void initializeWeightBoard() {
	    for(int i=0; i<size;i++){
			for(int j =0; j<size;j++){

			    weightBoard[i][j]=hasNeighbours(i,j).size()*(-0.1);
                //System.out.println(hasNeighbours(i,j).size());
                //for(int ii = 0; ii<hasNeighbours(i,j).size();ii++)
                  //  weightBoard[i][j]+=getLineLength(i,j, hasNeighbours(i,j).get(ii))*(-1);
				if(board[i][j] != null)
					weightBoard[i][j] = 0;
			}
		}
	}

	private ArrayList<Integer> hasNeighbours(int x, int y)
	{		
		ArrayList<Integer> directions = new ArrayList<Integer>();
		for(int i = 0; i<m.length;i++)
		{
			int cur_x=x+m[i][0];
			int cur_y=y+m[i][1];
			if(cur_x>=0 && cur_x < size && cur_y>=0 && cur_y < size)
			if(board[cur_x][cur_y] != null)
			{
				directions.add(i);
			}
		}
		return directions;
	}
}